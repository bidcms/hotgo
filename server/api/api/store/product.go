package store

import (
	"hotgo/internal/model/input/form"
	"hotgo/internal/model/input/store/product"

	"github.com/gogf/gf/v2/frame/g"
)

/*
"id": 4,

"relation_id": 0,
"type": 0,
"pid": 0,
"delivery_type": [

	"1"

],
"product_type": 0,
"store_name": "HUAWEI P50 Pro 4G全网通 原色双影像单元 麒麟9000芯片 万象双环设计 8GB+256GB曜金黑手机（测试商品，购买不发货）",
"cate_id": "67,66,65,4",
"image": "https:\/\/qiniu.crmeb.net\/attach\/2021\/10\/18\/4862d051ede43e7c0b7b6bc372ef12ef.jpg",
"sales": "0",
"price": "12999.00",
"stock": 54000,
"activity": [],
"ot_price": "12999.00",
"spec_type": 1,
"recommend_image": "",
"unit_name": "件",
"is_vip": 0,
"vip_price": 0,
"is_presale_product": 0,
"is_vip_product": 0,
"custom_form": [],
"presale_start_time": 0,
"presale_end_time": 0,
"is_limit": 0,
"limit_num": 0,
"star": "3.0",
"price_type": "",
"level_name": "",
"cart_button": 1,
"presale_pay_status": 0,
"checkCoupon": false,
"product_id": 4,
"promotions": []
*/
type ProductListReq struct {
	g.Meta `path:"/products" method:"get" tags:"商品" summary:"商品列表"`
	product.StoreProductListInp
}
type ProductDetailReq struct {
	g.Meta `path:"/product/detail/:id/*type" method:"get" tags:"商品" summary:"商品详情"`
}
type ProductDetailRes struct {
	product.StoreProduct
}

type ProductListRes struct {
	form.PageRes
	List []product.StoreProduct `json:"list"   dc:"数据列表"`
}
