package store

import (
	"context"
	storeapi "hotgo/api/api/store"
	"hotgo/internal/logic/product/product"
)

type StoreProductController struct{}

func (c *StoreProductController) List(ctx context.Context, req *storeapi.ProductListReq) (res storeapi.ProductListRes, err error) {

	var storeProductServices product.StoreProductServices
	res = storeapi.ProductListRes{}
	list, totalCount, _ := storeProductServices.GetGoodsList(ctx, req.StoreProductListInp)
	res.List = list
	res.PageRes.Pack(req, totalCount)
	return
}

func (c *StoreProductController) Detail(ctx context.Context, req *storeapi.ProductDetailReq) (res *storeapi.ProductDetailRes, err error) {

	return
}
