// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreCategoryDao is the data access object for table hg_store_category.
type StoreCategoryDao struct {
	table   string               // table is the underlying table name of the DAO.
	group   string               // group is the database configuration group name of current DAO.
	columns StoreCategoryColumns // columns contains all the column names of Table for convenient usage.
}

// StoreCategoryColumns defines and stores column names for table hg_store_category.
type StoreCategoryColumns struct {
	Id         string // 商品分类表ID
	Pid        string // 父id
	Type       string // 商品所属：0：平台1:门店2:供应商
	RelationId string // 关联门店、供应商ID
	CateName   string // 分类名称
	Sort       string // 排序
	Pic        string // 图标
	IsShow     string // 是否推荐
	AddTime    string // 添加时间
	BigPic     string // 分类大图
}

// storeCategoryColumns holds the columns for table hg_store_category.
var storeCategoryColumns = StoreCategoryColumns{
	Id:         "id",
	Pid:        "pid",
	Type:       "type",
	RelationId: "relation_id",
	CateName:   "cate_name",
	Sort:       "sort",
	Pic:        "pic",
	IsShow:     "is_show",
	AddTime:    "add_time",
	BigPic:     "big_pic",
}

// NewStoreCategoryDao creates and returns a new DAO object for table data access.
func NewStoreCategoryDao() *StoreCategoryDao {
	return &StoreCategoryDao{
		group:   "default",
		table:   "hg_store_category",
		columns: storeCategoryColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreCategoryDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreCategoryDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreCategoryDao) Columns() StoreCategoryColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreCategoryDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreCategoryDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreCategoryDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
