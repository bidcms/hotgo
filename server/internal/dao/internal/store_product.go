// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductDao is the data access object for table hg_store_product.
type StoreProductDao struct {
	table   string              // table is the underlying table name of the DAO.
	group   string              // group is the database configuration group name of current DAO.
	columns StoreProductColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductColumns defines and stores column names for table hg_store_product.
type StoreProductColumns struct {
	Id               string // 商品id
	Pid              string // 关联平台商品ID
	Type             string // 商品所属：0：平台1:门店2:供应商
	ProductType      string // 商品类型0:普通商品，1：卡密，2：优惠券，3：虚拟商品
	RelationId       string // 关联门店、供应商ID
	MerId            string // 商户Id(0为总后台管理员创建,不为0的时候是商户后台创建)
	Image            string // 商品图片
	RecommendImage   string // 推荐图
	SliderImage      string // 轮播图
	StoreName        string // 商品名称
	StoreInfo        string // 商品简介
	Keyword          string // 关键字
	BarCode          string // 商品条码（一维码）
	CateId           string // 分类id
	Price            string // 商品价格
	VipPrice         string // 会员价格
	OtPrice          string // 市场价
	Postage          string // 邮费
	DeliveryType     string // 商品配送方式
	Freight          string // 运费设置
	UnitName         string // 单位名
	Sort             string // 排序
	Sales            string // 销量
	Stock            string // 库存
	IsShow           string // 状态（0：未上架，1：上架）
	IsHot            string // 是否热卖
	IsBenefit        string // 是否优惠
	IsBest           string // 是否精品
	IsNew            string // 是否新品
	AddTime          string // 添加时间
	IsPostage        string // 是否包邮
	IsVerify         string // 是否审核：-2强制下架-1未通过0未审核1:通过
	IsDel            string // 是否删除
	MerUse           string // 商户是否代理 0不可代理1可代理
	GiveIntegral     string // 获得积分
	Cost             string // 成本价
	IsSeckill        string // 秒杀状态 0 未开启 1已开启
	IsBargain        string // 砍价状态 0未开启 1开启
	IsGood           string // 是否优品推荐
	IsSub            string // 是否单独分佣
	IsVip            string // 是否开启会员价格
	Ficti            string // 虚拟销量
	Browse           string // 浏览量
	CodePath         string // 商品二维码地址(用户小程序海报)
	SoureLink        string // 淘宝京东1688类型
	VideoLink        string // 主图视频链接
	TempId           string // 运费模板ID
	SpecType         string // 规格 0单 1多
	Activity         string // 活动显示排序1=秒杀，2=砍价，3=拼团
	Spu              string // 商品SPU
	LabelId          string // 标签ID
	CommandWord      string // 复制口令
	RecommendList    string // 推荐产品
	BrandId          string // 品牌id
	BrandCom         string // 品牌组合
	Code             string // 编码
	IsVipProduct     string // 是否会员专属商品
	IsPresaleProduct string // 是否预售商品
	PresaleStartTime string // 预售开始时间
	PresaleEndTime   string // 预售结束时间
	PresaleDay       string // 预售结束后几天内发货
	AutoOnTime       string // 自动上架时间
	AutoOffTime      string // 自动下架时间
	CustomForm       string // 自定义表单
	IsSupportRefund  string // 是否支持退款
	StoreLabelId     string // 商品标签iDS
	EnsureId         string // 商品保障服务ids
	Specs            string // 商品参数
	SpecsId          string // 参数模版ID
	IsLimit          string // 是否开启限购
	LimitType        string // 限购类型1单次限购2永久限购
	LimitNum         string // 限购数量
	Refusal          string // 审核拒绝原因
}

// storeProductColumns holds the columns for table hg_store_product.
var storeProductColumns = StoreProductColumns{
	Id:               "id",
	Pid:              "pid",
	Type:             "type",
	ProductType:      "product_type",
	RelationId:       "relation_id",
	MerId:            "mer_id",
	Image:            "image",
	RecommendImage:   "recommend_image",
	SliderImage:      "slider_image",
	StoreName:        "store_name",
	StoreInfo:        "store_info",
	Keyword:          "keyword",
	BarCode:          "bar_code",
	CateId:           "cate_id",
	Price:            "price",
	VipPrice:         "vip_price",
	OtPrice:          "ot_price",
	Postage:          "postage",
	DeliveryType:     "delivery_type",
	Freight:          "freight",
	UnitName:         "unit_name",
	Sort:             "sort",
	Sales:            "sales",
	Stock:            "stock",
	IsShow:           "is_show",
	IsHot:            "is_hot",
	IsBenefit:        "is_benefit",
	IsBest:           "is_best",
	IsNew:            "is_new",
	AddTime:          "add_time",
	IsPostage:        "is_postage",
	IsVerify:         "is_verify",
	IsDel:            "is_del",
	MerUse:           "mer_use",
	GiveIntegral:     "give_integral",
	Cost:             "cost",
	IsSeckill:        "is_seckill",
	IsBargain:        "is_bargain",
	IsGood:           "is_good",
	IsSub:            "is_sub",
	IsVip:            "is_vip",
	Ficti:            "ficti",
	Browse:           "browse",
	CodePath:         "code_path",
	SoureLink:        "soure_link",
	VideoLink:        "video_link",
	TempId:           "temp_id",
	SpecType:         "spec_type",
	Activity:         "activity",
	Spu:              "spu",
	LabelId:          "label_id",
	CommandWord:      "command_word",
	RecommendList:    "recommend_list",
	BrandId:          "brand_id",
	BrandCom:         "brand_com",
	Code:             "code",
	IsVipProduct:     "is_vip_product",
	IsPresaleProduct: "is_presale_product",
	PresaleStartTime: "presale_start_time",
	PresaleEndTime:   "presale_end_time",
	PresaleDay:       "presale_day",
	AutoOnTime:       "auto_on_time",
	AutoOffTime:      "auto_off_time",
	CustomForm:       "custom_form",
	IsSupportRefund:  "is_support_refund",
	StoreLabelId:     "store_label_id",
	EnsureId:         "ensure_id",
	Specs:            "specs",
	SpecsId:          "specs_id",
	IsLimit:          "is_limit",
	LimitType:        "limit_type",
	LimitNum:         "limit_num",
	Refusal:          "refusal",
}

// NewStoreProductDao creates and returns a new DAO object for table data access.
func NewStoreProductDao() *StoreProductDao {
	return &StoreProductDao{
		group:   "default",
		table:   "hg_store_product",
		columns: storeProductColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductDao) Columns() StoreProductColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
