// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductAttrDao is the data access object for table hg_store_product_attr.
type StoreProductAttrDao struct {
	table   string                  // table is the underlying table name of the DAO.
	group   string                  // group is the database configuration group name of current DAO.
	columns StoreProductAttrColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductAttrColumns defines and stores column names for table hg_store_product_attr.
type StoreProductAttrColumns struct {
	Id         string //
	ProductId  string // 商品ID
	AttrName   string // 属性名
	AttrValues string // 属性值
	Type       string // 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
}

// storeProductAttrColumns holds the columns for table hg_store_product_attr.
var storeProductAttrColumns = StoreProductAttrColumns{
	Id:         "id",
	ProductId:  "product_id",
	AttrName:   "attr_name",
	AttrValues: "attr_values",
	Type:       "type",
}

// NewStoreProductAttrDao creates and returns a new DAO object for table data access.
func NewStoreProductAttrDao() *StoreProductAttrDao {
	return &StoreProductAttrDao{
		group:   "default",
		table:   "hg_store_product_attr",
		columns: storeProductAttrColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductAttrDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductAttrDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductAttrDao) Columns() StoreProductAttrColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductAttrDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductAttrDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductAttrDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
