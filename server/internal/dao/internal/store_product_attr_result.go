// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductAttrResultDao is the data access object for table hg_store_product_attr_result.
type StoreProductAttrResultDao struct {
	table   string                        // table is the underlying table name of the DAO.
	group   string                        // group is the database configuration group name of current DAO.
	columns StoreProductAttrResultColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductAttrResultColumns defines and stores column names for table hg_store_product_attr_result.
type StoreProductAttrResultColumns struct {
	Id         string //
	ProductId  string // 商品ID
	Result     string // 商品属性参数
	ChangeTime string // 上次修改时间
	Type       string // 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
}

// storeProductAttrResultColumns holds the columns for table hg_store_product_attr_result.
var storeProductAttrResultColumns = StoreProductAttrResultColumns{
	Id:         "id",
	ProductId:  "product_id",
	Result:     "result",
	ChangeTime: "change_time",
	Type:       "type",
}

// NewStoreProductAttrResultDao creates and returns a new DAO object for table data access.
func NewStoreProductAttrResultDao() *StoreProductAttrResultDao {
	return &StoreProductAttrResultDao{
		group:   "default",
		table:   "hg_store_product_attr_result",
		columns: storeProductAttrResultColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductAttrResultDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductAttrResultDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductAttrResultDao) Columns() StoreProductAttrResultColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductAttrResultDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductAttrResultDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductAttrResultDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
