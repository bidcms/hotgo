// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductAttrValueDao is the data access object for table hg_store_product_attr_value.
type StoreProductAttrValueDao struct {
	table   string                       // table is the underlying table name of the DAO.
	group   string                       // group is the database configuration group name of current DAO.
	columns StoreProductAttrValueColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductAttrValueColumns defines and stores column names for table hg_store_product_attr_value.
type StoreProductAttrValueColumns struct {
	Id           string //
	ProductId    string // 商品ID
	ProductType  string // 商品类型
	Suk          string // 商品属性索引值 (attr_value|attr_value[|....])
	Stock        string // 属性对应的库存
	SumStock     string // 平台库存+门店库存总和
	Sales        string // 销量
	Price        string // 属性金额
	Image        string // 图片
	Unique       string // 唯一值
	Cost         string // 成本价
	BarCode      string // 商品条码
	OtPrice      string // 原价
	VipPrice     string // 会员专享价
	Weight       string // 重量
	Volume       string // 体积
	Brokerage    string // 一级返佣
	BrokerageTwo string // 二级返佣
	Type         string // 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
	Quota        string // 活动限购数量
	QuotaShow    string // 活动限购数量显示
	Code         string // 编码
	DiskInfo     string // 虚拟信息内容
}

// storeProductAttrValueColumns holds the columns for table hg_store_product_attr_value.
var storeProductAttrValueColumns = StoreProductAttrValueColumns{
	Id:           "id",
	ProductId:    "product_id",
	ProductType:  "product_type",
	Suk:          "suk",
	Stock:        "stock",
	SumStock:     "sum_stock",
	Sales:        "sales",
	Price:        "price",
	Image:        "image",
	Unique:       "unique",
	Cost:         "cost",
	BarCode:      "bar_code",
	OtPrice:      "ot_price",
	VipPrice:     "vip_price",
	Weight:       "weight",
	Volume:       "volume",
	Brokerage:    "brokerage",
	BrokerageTwo: "brokerage_two",
	Type:         "type",
	Quota:        "quota",
	QuotaShow:    "quota_show",
	Code:         "code",
	DiskInfo:     "disk_info",
}

// NewStoreProductAttrValueDao creates and returns a new DAO object for table data access.
func NewStoreProductAttrValueDao() *StoreProductAttrValueDao {
	return &StoreProductAttrValueDao{
		group:   "default",
		table:   "hg_store_product_attr_value",
		columns: storeProductAttrValueColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductAttrValueDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductAttrValueDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductAttrValueDao) Columns() StoreProductAttrValueColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductAttrValueDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductAttrValueDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductAttrValueDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
