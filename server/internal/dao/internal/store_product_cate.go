// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductCateDao is the data access object for table hg_store_product_cate.
type StoreProductCateDao struct {
	table   string                  // table is the underlying table name of the DAO.
	group   string                  // group is the database configuration group name of current DAO.
	columns StoreProductCateColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductCateColumns defines and stores column names for table hg_store_product_cate.
type StoreProductCateColumns struct {
	Id        string //
	ProductId string // 商品id
	CateId    string // 分类id
	AddTime   string // 添加时间
	CatePid   string // 一级分类id
	Status    string // 商品状态
}

// storeProductCateColumns holds the columns for table hg_store_product_cate.
var storeProductCateColumns = StoreProductCateColumns{
	Id:        "id",
	ProductId: "product_id",
	CateId:    "cate_id",
	AddTime:   "add_time",
	CatePid:   "cate_pid",
	Status:    "status",
}

// NewStoreProductCateDao creates and returns a new DAO object for table data access.
func NewStoreProductCateDao() *StoreProductCateDao {
	return &StoreProductCateDao{
		group:   "default",
		table:   "hg_store_product_cate",
		columns: storeProductCateColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductCateDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductCateDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductCateDao) Columns() StoreProductCateColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductCateDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductCateDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductCateDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
