// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductCouponDao is the data access object for table hg_store_product_coupon.
type StoreProductCouponDao struct {
	table   string                    // table is the underlying table name of the DAO.
	group   string                    // group is the database configuration group name of current DAO.
	columns StoreProductCouponColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductCouponColumns defines and stores column names for table hg_store_product_coupon.
type StoreProductCouponColumns struct {
	Id            string //
	ProductId     string // 商品id
	IssueCouponId string // 优惠劵id
	AddTime       string // 添加时间
	Title         string // 优惠券名称
}

// storeProductCouponColumns holds the columns for table hg_store_product_coupon.
var storeProductCouponColumns = StoreProductCouponColumns{
	Id:            "id",
	ProductId:     "product_id",
	IssueCouponId: "issue_coupon_id",
	AddTime:       "add_time",
	Title:         "title",
}

// NewStoreProductCouponDao creates and returns a new DAO object for table data access.
func NewStoreProductCouponDao() *StoreProductCouponDao {
	return &StoreProductCouponDao{
		group:   "default",
		table:   "hg_store_product_coupon",
		columns: storeProductCouponColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductCouponDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductCouponDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductCouponDao) Columns() StoreProductCouponColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductCouponDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductCouponDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductCouponDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
