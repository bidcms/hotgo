// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductDescriptionDao is the data access object for table hg_store_product_description.
type StoreProductDescriptionDao struct {
	table   string                         // table is the underlying table name of the DAO.
	group   string                         // group is the database configuration group name of current DAO.
	columns StoreProductDescriptionColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductDescriptionColumns defines and stores column names for table hg_store_product_description.
type StoreProductDescriptionColumns struct {
	ProductId   string // 商品ID
	Description string // 商品详情
	Type        string // 商品类型
}

// storeProductDescriptionColumns holds the columns for table hg_store_product_description.
var storeProductDescriptionColumns = StoreProductDescriptionColumns{
	ProductId:   "product_id",
	Description: "description",
	Type:        "type",
}

// NewStoreProductDescriptionDao creates and returns a new DAO object for table data access.
func NewStoreProductDescriptionDao() *StoreProductDescriptionDao {
	return &StoreProductDescriptionDao{
		group:   "default",
		table:   "hg_store_product_description",
		columns: storeProductDescriptionColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductDescriptionDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductDescriptionDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductDescriptionDao) Columns() StoreProductDescriptionColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductDescriptionDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductDescriptionDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductDescriptionDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
