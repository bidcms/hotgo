// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductEnsureDao is the data access object for table hg_store_product_ensure.
type StoreProductEnsureDao struct {
	table   string                    // table is the underlying table name of the DAO.
	group   string                    // group is the database configuration group name of current DAO.
	columns StoreProductEnsureColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductEnsureColumns defines and stores column names for table hg_store_product_ensure.
type StoreProductEnsureColumns struct {
	Id      string //
	Type    string // 类型：1平台2:门店
	StoreId string // 门店ID
	Name    string // 标签名称
	Image   string // 图片
	Desc    string // 描述
	Sort    string // 排序
	Status  string // 状态
	AddTime string // 添加时间
}

// storeProductEnsureColumns holds the columns for table hg_store_product_ensure.
var storeProductEnsureColumns = StoreProductEnsureColumns{
	Id:      "id",
	Type:    "type",
	StoreId: "store_id",
	Name:    "name",
	Image:   "image",
	Desc:    "desc",
	Sort:    "sort",
	Status:  "status",
	AddTime: "add_time",
}

// NewStoreProductEnsureDao creates and returns a new DAO object for table data access.
func NewStoreProductEnsureDao() *StoreProductEnsureDao {
	return &StoreProductEnsureDao{
		group:   "default",
		table:   "hg_store_product_ensure",
		columns: storeProductEnsureColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductEnsureDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductEnsureDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductEnsureDao) Columns() StoreProductEnsureColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductEnsureDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductEnsureDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductEnsureDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
