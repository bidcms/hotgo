// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductLabelDao is the data access object for table hg_store_product_label.
type StoreProductLabelDao struct {
	table   string                   // table is the underlying table name of the DAO.
	group   string                   // group is the database configuration group name of current DAO.
	columns StoreProductLabelColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductLabelColumns defines and stores column names for table hg_store_product_label.
type StoreProductLabelColumns struct {
	Id        string //
	Type      string // 类型：1平台2:门店
	StoreId   string // 门店ID
	LabelCate string // 标签分类
	LabelName string // 标签名称
	Sort      string // 排序
	AddTime   string // 添加时间
}

// storeProductLabelColumns holds the columns for table hg_store_product_label.
var storeProductLabelColumns = StoreProductLabelColumns{
	Id:        "id",
	Type:      "type",
	StoreId:   "store_id",
	LabelCate: "label_cate",
	LabelName: "label_name",
	Sort:      "sort",
	AddTime:   "add_time",
}

// NewStoreProductLabelDao creates and returns a new DAO object for table data access.
func NewStoreProductLabelDao() *StoreProductLabelDao {
	return &StoreProductLabelDao{
		group:   "default",
		table:   "hg_store_product_label",
		columns: storeProductLabelColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductLabelDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductLabelDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductLabelDao) Columns() StoreProductLabelColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductLabelDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductLabelDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductLabelDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
