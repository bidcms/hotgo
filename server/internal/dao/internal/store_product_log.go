// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductLogDao is the data access object for table hg_store_product_log.
type StoreProductLogDao struct {
	table   string                 // table is the underlying table name of the DAO.
	group   string                 // group is the database configuration group name of current DAO.
	columns StoreProductLogColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductLogColumns defines and stores column names for table hg_store_product_log.
type StoreProductLogColumns struct {
	Id          string //
	Type        string // 类型
	ProductId   string // 商品ID
	Uid         string // 用户ID
	VisitNum    string // 是否浏览
	CartNum     string // 加入购物车数量
	OrderNum    string // 下单数量
	PayNum      string // 支付数量
	PayPrice    string // 支付金额
	CostPrice   string // 商品成本价
	PayUid      string // 支付用户ID
	RefundNum   string // 退款数量
	RefundPrice string // 退款金额
	CollectNum  string // 收藏
	AddTime     string // 添加时间
	DeleteTime  string // 删除时间
}

// storeProductLogColumns holds the columns for table hg_store_product_log.
var storeProductLogColumns = StoreProductLogColumns{
	Id:          "id",
	Type:        "type",
	ProductId:   "product_id",
	Uid:         "uid",
	VisitNum:    "visit_num",
	CartNum:     "cart_num",
	OrderNum:    "order_num",
	PayNum:      "pay_num",
	PayPrice:    "pay_price",
	CostPrice:   "cost_price",
	PayUid:      "pay_uid",
	RefundNum:   "refund_num",
	RefundPrice: "refund_price",
	CollectNum:  "collect_num",
	AddTime:     "add_time",
	DeleteTime:  "delete_time",
}

// NewStoreProductLogDao creates and returns a new DAO object for table data access.
func NewStoreProductLogDao() *StoreProductLogDao {
	return &StoreProductLogDao{
		group:   "default",
		table:   "hg_store_product_log",
		columns: storeProductLogColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductLogDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductLogDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductLogDao) Columns() StoreProductLogColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductLogDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductLogDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductLogDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
