// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductReplyDao is the data access object for table hg_store_product_reply.
type StoreProductReplyDao struct {
	table   string                   // table is the underlying table name of the DAO.
	group   string                   // group is the database configuration group name of current DAO.
	columns StoreProductReplyColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductReplyColumns defines and stores column names for table hg_store_product_reply.
type StoreProductReplyColumns struct {
	Id                   string // 评论ID
	Uid                  string // 用户ID
	Oid                  string // 订单ID
	Type                 string // 类型：1平台2:门店
	StoreId              string // 门店id
	Unique               string // 唯一id
	ProductId            string // 商品id
	ReplyType            string // 某种商品类型(普通商品、秒杀商品）
	ProductScore         string // 商品分数
	ServiceScore         string // 服务分数
	Comment              string // 评论内容
	Pics                 string // 评论图片
	AddTime              string // 评论时间
	Praise               string // 点赞
	ViewsNum             string // 浏览量
	MerchantReplyContent string // 管理员回复内容
	MerchantReplyTime    string // 管理员回复时间
	IsDel                string // 0未删除1已删除
	IsReply              string // 0未回复1已回复
	Nickname             string // 用户名称
	Avatar               string // 用户头像
}

// storeProductReplyColumns holds the columns for table hg_store_product_reply.
var storeProductReplyColumns = StoreProductReplyColumns{
	Id:                   "id",
	Uid:                  "uid",
	Oid:                  "oid",
	Type:                 "type",
	StoreId:              "store_id",
	Unique:               "unique",
	ProductId:            "product_id",
	ReplyType:            "reply_type",
	ProductScore:         "product_score",
	ServiceScore:         "service_score",
	Comment:              "comment",
	Pics:                 "pics",
	AddTime:              "add_time",
	Praise:               "praise",
	ViewsNum:             "views_num",
	MerchantReplyContent: "merchant_reply_content",
	MerchantReplyTime:    "merchant_reply_time",
	IsDel:                "is_del",
	IsReply:              "is_reply",
	Nickname:             "nickname",
	Avatar:               "avatar",
}

// NewStoreProductReplyDao creates and returns a new DAO object for table data access.
func NewStoreProductReplyDao() *StoreProductReplyDao {
	return &StoreProductReplyDao{
		group:   "default",
		table:   "hg_store_product_reply",
		columns: storeProductReplyColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductReplyDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductReplyDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductReplyDao) Columns() StoreProductReplyColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductReplyDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductReplyDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductReplyDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
