// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductReplyCommentDao is the data access object for table hg_store_product_reply_comment.
type StoreProductReplyCommentDao struct {
	table   string                          // table is the underlying table name of the DAO.
	group   string                          // group is the database configuration group name of current DAO.
	columns StoreProductReplyCommentColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductReplyCommentColumns defines and stores column names for table hg_store_product_reply_comment.
type StoreProductReplyCommentColumns struct {
	Id         string //
	Uid        string //
	Praise     string // 点赞数量
	StoreId    string // 门店id
	Content    string // 回复内容
	Pid        string // 上级回复id
	ReplyId    string // 评论id
	CreateTime string // 创建时间
	UpdateTime string // 更新时间
}

// storeProductReplyCommentColumns holds the columns for table hg_store_product_reply_comment.
var storeProductReplyCommentColumns = StoreProductReplyCommentColumns{
	Id:         "id",
	Uid:        "uid",
	Praise:     "praise",
	StoreId:    "store_id",
	Content:    "content",
	Pid:        "pid",
	ReplyId:    "reply_id",
	CreateTime: "create_time",
	UpdateTime: "update_time",
}

// NewStoreProductReplyCommentDao creates and returns a new DAO object for table data access.
func NewStoreProductReplyCommentDao() *StoreProductReplyCommentDao {
	return &StoreProductReplyCommentDao{
		group:   "default",
		table:   "hg_store_product_reply_comment",
		columns: storeProductReplyCommentColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductReplyCommentDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductReplyCommentDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductReplyCommentDao) Columns() StoreProductReplyCommentColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductReplyCommentDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductReplyCommentDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductReplyCommentDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
