// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductSpecsDao is the data access object for table hg_store_product_specs.
type StoreProductSpecsDao struct {
	table   string                   // table is the underlying table name of the DAO.
	group   string                   // group is the database configuration group name of current DAO.
	columns StoreProductSpecsColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductSpecsColumns defines and stores column names for table hg_store_product_specs.
type StoreProductSpecsColumns struct {
	Id      string //
	Type    string // 类型：1平台2:门店
	StoreId string // 门店ID
	TempId  string // 模版ID
	Name    string // 参数名称
	Value   string // 参数值
	Sort    string // 排序
	Status  string // 状态
	AddTime string // 添加时间
}

// storeProductSpecsColumns holds the columns for table hg_store_product_specs.
var storeProductSpecsColumns = StoreProductSpecsColumns{
	Id:      "id",
	Type:    "type",
	StoreId: "store_id",
	TempId:  "temp_id",
	Name:    "name",
	Value:   "value",
	Sort:    "sort",
	Status:  "status",
	AddTime: "add_time",
}

// NewStoreProductSpecsDao creates and returns a new DAO object for table data access.
func NewStoreProductSpecsDao() *StoreProductSpecsDao {
	return &StoreProductSpecsDao{
		group:   "default",
		table:   "hg_store_product_specs",
		columns: storeProductSpecsColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductSpecsDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductSpecsDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductSpecsDao) Columns() StoreProductSpecsColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductSpecsDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductSpecsDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductSpecsDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
