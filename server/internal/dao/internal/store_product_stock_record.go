// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductStockRecordDao is the data access object for table hg_store_product_stock_record.
type StoreProductStockRecordDao struct {
	table   string                         // table is the underlying table name of the DAO.
	group   string                         // group is the database configuration group name of current DAO.
	columns StoreProductStockRecordColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductStockRecordColumns defines and stores column names for table hg_store_product_stock_record.
type StoreProductStockRecordColumns struct {
	Id        string //
	StoreId   string // 门店ID
	ProductId string // 商品ID
	Unique    string // 规格唯一值
	CostPrice string // 成本价
	Number    string // 数量
	Pm        string // 1:入库0:出库
	AddTime   string // 时间
}

// storeProductStockRecordColumns holds the columns for table hg_store_product_stock_record.
var storeProductStockRecordColumns = StoreProductStockRecordColumns{
	Id:        "id",
	StoreId:   "store_id",
	ProductId: "product_id",
	Unique:    "unique",
	CostPrice: "cost_price",
	Number:    "number",
	Pm:        "pm",
	AddTime:   "add_time",
}

// NewStoreProductStockRecordDao creates and returns a new DAO object for table data access.
func NewStoreProductStockRecordDao() *StoreProductStockRecordDao {
	return &StoreProductStockRecordDao{
		group:   "default",
		table:   "hg_store_product_stock_record",
		columns: storeProductStockRecordColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductStockRecordDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductStockRecordDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductStockRecordDao) Columns() StoreProductStockRecordColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductStockRecordDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductStockRecordDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductStockRecordDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
