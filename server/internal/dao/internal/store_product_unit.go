// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductUnitDao is the data access object for table hg_store_product_unit.
type StoreProductUnitDao struct {
	table   string                  // table is the underlying table name of the DAO.
	group   string                  // group is the database configuration group name of current DAO.
	columns StoreProductUnitColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductUnitColumns defines and stores column names for table hg_store_product_unit.
type StoreProductUnitColumns struct {
	Id      string //
	StoreId string // 门店ID
	Name    string // 单位名称
	Sort    string // 排序
	Status  string // 是否上架
	IsDel   string // 是否删除
	AddTime string // 添加时间
}

// storeProductUnitColumns holds the columns for table hg_store_product_unit.
var storeProductUnitColumns = StoreProductUnitColumns{
	Id:      "id",
	StoreId: "store_id",
	Name:    "name",
	Sort:    "sort",
	Status:  "status",
	IsDel:   "is_del",
	AddTime: "add_time",
}

// NewStoreProductUnitDao creates and returns a new DAO object for table data access.
func NewStoreProductUnitDao() *StoreProductUnitDao {
	return &StoreProductUnitDao{
		group:   "default",
		table:   "hg_store_product_unit",
		columns: storeProductUnitColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductUnitDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductUnitDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductUnitDao) Columns() StoreProductUnitColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductUnitDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductUnitDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductUnitDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
