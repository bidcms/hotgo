// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductVirtualDao is the data access object for table hg_store_product_virtual.
type StoreProductVirtualDao struct {
	table   string                     // table is the underlying table name of the DAO.
	group   string                     // group is the database configuration group name of current DAO.
	columns StoreProductVirtualColumns // columns contains all the column names of Table for convenient usage.
}

// StoreProductVirtualColumns defines and stores column names for table hg_store_product_virtual.
type StoreProductVirtualColumns struct {
	Id         string // 主键id
	ProductId  string // 商品id
	StoreId    string // 门店id
	AttrUnique string // 对应商品规格
	CardNo     string // 卡密卡号
	CardPwd    string // 卡密密码
	CardUnique string // 虚拟卡密唯一值
	OrderId    string // 购买订单id
	OrderType  string // 购买订单类型：1:order 2:积分订单
	Uid        string // 购买人id
}

// storeProductVirtualColumns holds the columns for table hg_store_product_virtual.
var storeProductVirtualColumns = StoreProductVirtualColumns{
	Id:         "id",
	ProductId:  "product_id",
	StoreId:    "store_id",
	AttrUnique: "attr_unique",
	CardNo:     "card_no",
	CardPwd:    "card_pwd",
	CardUnique: "card_unique",
	OrderId:    "order_id",
	OrderType:  "order_type",
	Uid:        "uid",
}

// NewStoreProductVirtualDao creates and returns a new DAO object for table data access.
func NewStoreProductVirtualDao() *StoreProductVirtualDao {
	return &StoreProductVirtualDao{
		group:   "default",
		table:   "hg_store_product_virtual",
		columns: storeProductVirtualColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *StoreProductVirtualDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *StoreProductVirtualDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *StoreProductVirtualDao) Columns() StoreProductVirtualColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *StoreProductVirtualDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *StoreProductVirtualDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *StoreProductVirtualDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
