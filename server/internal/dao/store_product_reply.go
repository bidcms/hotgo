// =================================================================================
// This is auto-generated by GoFrame CLI tool only once. Fill this file as you wish.
// =================================================================================

package dao

import (
	"hotgo/internal/dao/internal"
)

// internalStoreProductReplyDao is internal type for wrapping internal DAO implements.
type internalStoreProductReplyDao = *internal.StoreProductReplyDao

// storeProductReplyDao is the data access object for table hg_store_product_reply.
// You can define custom methods on it to extend its functionality as you wish.
type storeProductReplyDao struct {
	internalStoreProductReplyDao
}

var (
	// StoreProductReply is globally public accessible object for table hg_store_product_reply operations.
	StoreProductReply = storeProductReplyDao{
		internal.NewStoreProductReplyDao(),
	}
)

// Fill with you ideas below.
