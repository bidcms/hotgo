package logic

import (
	"context"
	"hotgo/internal/library/token"
	"hotgo/internal/model"

	"github.com/gogf/gf/v2/crypto/gaes"
	"github.com/gogf/gf/v2/crypto/gmd5"
	"github.com/gogf/gf/v2/net/ghttp"
)

type BaseLogic struct {
}

/**
 * 获取分页配置
 * @param bool $isPage
 * @param bool $isRelieve
 * @return int[]
 */
func (b *BaseLogic) getPageValue(ctx context.Context, isPage bool, isRelieve bool) (page int, limit int, defaultLimit int) {
	if isPage {
		request := ghttp.RequestFromCtx(ctx)
		// config, _ := service.SysConfig().GetBasic(ctx)
		page = request.Get("page").Int()
		limit = request.Get("limit").Int()
	}
	limitMax := 100
	defaultLimit = 10
	if limit > limitMax && isRelieve {
		limit = limitMax
	}
	return
}

/**
 * 创建token
 * @param int $id
 * @param $type
 * @return array
 */
func CreateToken(ctx context.Context, user model.Identity) (lt string, expires int64, err error) {
	lt, expires, err = token.Login(ctx, &user)
	return
}

/**
 * 获取路由地址
 * @param string $path
 * @param array $params
 * @param bool $suffix
 * @return \think\route\Url
 */
func url(ctx context.Context, path string, params []string, suffix bool, isDomain bool) string {
	return ghttp.RequestFromCtx(ctx).Router.Uri
}

/**
 * 密码hash加密
 * @param string $password
 * @return false|string|null
 */
func passwordHash(ctx context.Context, password string, method string) (hash string) {
	hash = gmd5.MustEncryptString(password)
	if method == "aes" {
		aes, _ := gaes.PKCS5UnPadding([]byte(password), 8)
		hash = string(aes)
	}
	return
}
