package product

import (
	"context"
	"hotgo/internal/dao"
	"hotgo/internal/model/input/store/product"

	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/text/gstr"
	"github.com/gogf/gf/v2/util/gconv"
)

type StoreProductServices struct {
}

func (s *StoreProductServices) GetGoodsList(ctx context.Context, req product.StoreProductListInp) (list []product.StoreProduct, totalCount int, err error) {

	where := g.Map{
		"is_verify":      1,
		"is_show":        1,
		"is_del":         0,
		"is_vip_product": 0,
	}
	Ids := g.Slice{}
	if req.Sid > 0 {
		//查询是否有父级
		pid, _ := dao.StoreCategory.Ctx(ctx).Where("id=?", req.Sid).Value("pid")
		if gconv.Int64(pid) > 0 {
			productIds, _ := dao.StoreProductCate.Ctx(ctx).Where("cate_id=?", pid).Fields("product_id").All()
			for _, v := range productIds {
				Ids = append(Ids, v["product_id"])
			}
		}
		where["id in(?)"] = Ids
	}
	if req.Cid > 0 {
		//查询所有子级
		cIds, _ := dao.StoreCategory.Ctx(ctx).Where("pid=?", req.Cid).Fields("id").All()
		idList := g.SliceInt{req.Cid}
		for _, v := range cIds {
			idList = append(idList, gconv.Int(v["id"]))
		}
		if len(idList) > 0 {
			productIds, _ := dao.StoreProductCate.Ctx(ctx).Where("cate_id in (?)", idList).Fields("product_id").All()
			for _, v := range productIds {
				Ids = append(Ids, v["product_id"])
			}
		}
		where["id in(?)"] = Ids
	}
	if req.Ids != "" {
		where["id in(?)"] = gstr.Split(req.Ids, ",")
	}
	if req.New > 0 {
		where["is_new"] = req.New
	}
	if req.BrandId != "" {
		where["brand_id in(?)"] = gstr.Split(req.BrandId, ",")
	}
	if req.Keyword != "" {
		where["store_name like ?"] = "%" + req.Keyword + "%"
	}
	list = []product.StoreProduct{}
	dao.StoreProduct.Ctx(ctx).Hook(product.Hook).WithAll().Where(where).Limit(10).ScanAndCount(&list, &totalCount, true)
	return
}
