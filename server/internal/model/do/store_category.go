// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreCategory is the golang structure of table hg_store_category for DAO operations like Where/Data.
type StoreCategory struct {
	g.Meta     `orm:"table:hg_store_category, do:true"`
	Id         interface{} // 商品分类表ID
	Pid        interface{} // 父id
	Type       interface{} // 商品所属：0：平台1:门店2:供应商
	RelationId interface{} // 关联门店、供应商ID
	CateName   interface{} // 分类名称
	Sort       interface{} // 排序
	Pic        interface{} // 图标
	IsShow     interface{} // 是否推荐
	AddTime    interface{} // 添加时间
	BigPic     interface{} // 分类大图
}
