// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProduct is the golang structure of table hg_store_product for DAO operations like Where/Data.
type StoreProduct struct {
	g.Meta           `orm:"table:hg_store_product, do:true"`
	Id               interface{} // 商品id
	Pid              interface{} // 关联平台商品ID
	Type             interface{} // 商品所属：0：平台1:门店2:供应商
	ProductType      interface{} // 商品类型0:普通商品，1：卡密，2：优惠券，3：虚拟商品
	RelationId       interface{} // 关联门店、供应商ID
	MerId            interface{} // 商户Id(0为总后台管理员创建,不为0的时候是商户后台创建)
	Image            interface{} // 商品图片
	RecommendImage   interface{} // 推荐图
	SliderImage      interface{} // 轮播图
	StoreName        interface{} // 商品名称
	StoreInfo        interface{} // 商品简介
	Keyword          interface{} // 关键字
	BarCode          interface{} // 商品条码（一维码）
	CateId           interface{} // 分类id
	Price            interface{} // 商品价格
	VipPrice         interface{} // 会员价格
	OtPrice          interface{} // 市场价
	Postage          interface{} // 邮费
	DeliveryType     interface{} // 商品配送方式
	Freight          interface{} // 运费设置
	UnitName         interface{} // 单位名
	Sort             interface{} // 排序
	Sales            interface{} // 销量
	Stock            interface{} // 库存
	IsShow           interface{} // 状态（0：未上架，1：上架）
	IsHot            interface{} // 是否热卖
	IsBenefit        interface{} // 是否优惠
	IsBest           interface{} // 是否精品
	IsNew            interface{} // 是否新品
	AddTime          interface{} // 添加时间
	IsPostage        interface{} // 是否包邮
	IsVerify         interface{} // 是否审核：-2强制下架-1未通过0未审核1:通过
	IsDel            interface{} // 是否删除
	MerUse           interface{} // 商户是否代理 0不可代理1可代理
	GiveIntegral     interface{} // 获得积分
	Cost             interface{} // 成本价
	IsSeckill        interface{} // 秒杀状态 0 未开启 1已开启
	IsBargain        interface{} // 砍价状态 0未开启 1开启
	IsGood           interface{} // 是否优品推荐
	IsSub            interface{} // 是否单独分佣
	IsVip            interface{} // 是否开启会员价格
	Ficti            interface{} // 虚拟销量
	Browse           interface{} // 浏览量
	CodePath         interface{} // 商品二维码地址(用户小程序海报)
	SoureLink        interface{} // 淘宝京东1688类型
	VideoLink        interface{} // 主图视频链接
	TempId           interface{} // 运费模板ID
	SpecType         interface{} // 规格 0单 1多
	Activity         interface{} // 活动显示排序1=秒杀，2=砍价，3=拼团
	Spu              interface{} // 商品SPU
	LabelId          interface{} // 标签ID
	CommandWord      interface{} // 复制口令
	RecommendList    interface{} // 推荐产品
	BrandId          interface{} // 品牌id
	BrandCom         interface{} // 品牌组合
	Code             interface{} // 编码
	IsVipProduct     interface{} // 是否会员专属商品
	IsPresaleProduct interface{} // 是否预售商品
	PresaleStartTime interface{} // 预售开始时间
	PresaleEndTime   interface{} // 预售结束时间
	PresaleDay       interface{} // 预售结束后几天内发货
	AutoOnTime       interface{} // 自动上架时间
	AutoOffTime      interface{} // 自动下架时间
	CustomForm       interface{} // 自定义表单
	IsSupportRefund  interface{} // 是否支持退款
	StoreLabelId     interface{} // 商品标签iDS
	EnsureId         interface{} // 商品保障服务ids
	Specs            interface{} // 商品参数
	SpecsId          interface{} // 参数模版ID
	IsLimit          interface{} // 是否开启限购
	LimitType        interface{} // 限购类型1单次限购2永久限购
	LimitNum         interface{} // 限购数量
	Refusal          interface{} // 审核拒绝原因
}
