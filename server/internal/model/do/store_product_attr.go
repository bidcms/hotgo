// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductAttr is the golang structure of table hg_store_product_attr for DAO operations like Where/Data.
type StoreProductAttr struct {
	g.Meta     `orm:"table:hg_store_product_attr, do:true"`
	Id         interface{} //
	ProductId  interface{} // 商品ID
	AttrName   interface{} // 属性名
	AttrValues interface{} // 属性值
	Type       interface{} // 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
}
