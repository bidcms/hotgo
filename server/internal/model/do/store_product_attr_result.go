// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductAttrResult is the golang structure of table hg_store_product_attr_result for DAO operations like Where/Data.
type StoreProductAttrResult struct {
	g.Meta     `orm:"table:hg_store_product_attr_result, do:true"`
	Id         interface{} //
	ProductId  interface{} // 商品ID
	Result     interface{} // 商品属性参数
	ChangeTime interface{} // 上次修改时间
	Type       interface{} // 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
}
