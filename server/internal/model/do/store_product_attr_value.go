// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductAttrValue is the golang structure of table hg_store_product_attr_value for DAO operations like Where/Data.
type StoreProductAttrValue struct {
	g.Meta       `orm:"table:hg_store_product_attr_value, do:true"`
	Id           interface{} //
	ProductId    interface{} // 商品ID
	ProductType  interface{} // 商品类型
	Suk          interface{} // 商品属性索引值 (attr_value|attr_value[|....])
	Stock        interface{} // 属性对应的库存
	SumStock     interface{} // 平台库存+门店库存总和
	Sales        interface{} // 销量
	Price        interface{} // 属性金额
	Image        interface{} // 图片
	Unique       interface{} // 唯一值
	Cost         interface{} // 成本价
	BarCode      interface{} // 商品条码
	OtPrice      interface{} // 原价
	VipPrice     interface{} // 会员专享价
	Weight       interface{} // 重量
	Volume       interface{} // 体积
	Brokerage    interface{} // 一级返佣
	BrokerageTwo interface{} // 二级返佣
	Type         interface{} // 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
	Quota        interface{} // 活动限购数量
	QuotaShow    interface{} // 活动限购数量显示
	Code         interface{} // 编码
	DiskInfo     interface{} // 虚拟信息内容
}
