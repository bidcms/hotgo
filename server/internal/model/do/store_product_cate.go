// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductCate is the golang structure of table hg_store_product_cate for DAO operations like Where/Data.
type StoreProductCate struct {
	g.Meta    `orm:"table:hg_store_product_cate, do:true"`
	Id        interface{} //
	ProductId interface{} // 商品id
	CateId    interface{} // 分类id
	AddTime   interface{} // 添加时间
	CatePid   interface{} // 一级分类id
	Status    interface{} // 商品状态
}
