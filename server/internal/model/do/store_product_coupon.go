// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductCoupon is the golang structure of table hg_store_product_coupon for DAO operations like Where/Data.
type StoreProductCoupon struct {
	g.Meta        `orm:"table:hg_store_product_coupon, do:true"`
	Id            interface{} //
	ProductId     interface{} // 商品id
	IssueCouponId interface{} // 优惠劵id
	AddTime       interface{} // 添加时间
	Title         interface{} // 优惠券名称
}
