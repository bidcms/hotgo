// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductDescription is the golang structure of table hg_store_product_description for DAO operations like Where/Data.
type StoreProductDescription struct {
	g.Meta      `orm:"table:hg_store_product_description, do:true"`
	ProductId   interface{} // 商品ID
	Description interface{} // 商品详情
	Type        interface{} // 商品类型
}
