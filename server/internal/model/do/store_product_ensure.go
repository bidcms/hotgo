// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductEnsure is the golang structure of table hg_store_product_ensure for DAO operations like Where/Data.
type StoreProductEnsure struct {
	g.Meta  `orm:"table:hg_store_product_ensure, do:true"`
	Id      interface{} //
	Type    interface{} // 类型：1平台2:门店
	StoreId interface{} // 门店ID
	Name    interface{} // 标签名称
	Image   interface{} // 图片
	Desc    interface{} // 描述
	Sort    interface{} // 排序
	Status  interface{} // 状态
	AddTime interface{} // 添加时间
}
