// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductLabel is the golang structure of table hg_store_product_label for DAO operations like Where/Data.
type StoreProductLabel struct {
	g.Meta    `orm:"table:hg_store_product_label, do:true"`
	Id        interface{} //
	Type      interface{} // 类型：1平台2:门店
	StoreId   interface{} // 门店ID
	LabelCate interface{} // 标签分类
	LabelName interface{} // 标签名称
	Sort      interface{} // 排序
	AddTime   interface{} // 添加时间
}
