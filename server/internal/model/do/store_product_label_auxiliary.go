// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductLabelAuxiliary is the golang structure of table hg_store_product_label_auxiliary for DAO operations like Where/Data.
type StoreProductLabelAuxiliary struct {
	g.Meta    `orm:"table:hg_store_product_label_auxiliary, do:true"`
	Id        interface{} //
	LabelId   interface{} //
	ProductId interface{} //
}
