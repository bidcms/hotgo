// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// StoreProductLog is the golang structure of table hg_store_product_log for DAO operations like Where/Data.
type StoreProductLog struct {
	g.Meta      `orm:"table:hg_store_product_log, do:true"`
	Id          interface{} //
	Type        interface{} // 类型
	ProductId   interface{} // 商品ID
	Uid         interface{} // 用户ID
	VisitNum    interface{} // 是否浏览
	CartNum     interface{} // 加入购物车数量
	OrderNum    interface{} // 下单数量
	PayNum      interface{} // 支付数量
	PayPrice    interface{} // 支付金额
	CostPrice   interface{} // 商品成本价
	PayUid      interface{} // 支付用户ID
	RefundNum   interface{} // 退款数量
	RefundPrice interface{} // 退款金额
	CollectNum  interface{} // 收藏
	AddTime     interface{} // 添加时间
	DeleteTime  *gtime.Time // 删除时间
}
