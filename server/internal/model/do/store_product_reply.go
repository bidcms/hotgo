// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductReply is the golang structure of table hg_store_product_reply for DAO operations like Where/Data.
type StoreProductReply struct {
	g.Meta               `orm:"table:hg_store_product_reply, do:true"`
	Id                   interface{} // 评论ID
	Uid                  interface{} // 用户ID
	Oid                  interface{} // 订单ID
	Type                 interface{} // 类型：1平台2:门店
	StoreId              interface{} // 门店id
	Unique               interface{} // 唯一id
	ProductId            interface{} // 商品id
	ReplyType            interface{} // 某种商品类型(普通商品、秒杀商品）
	ProductScore         interface{} // 商品分数
	ServiceScore         interface{} // 服务分数
	Comment              interface{} // 评论内容
	Pics                 interface{} // 评论图片
	AddTime              interface{} // 评论时间
	Praise               interface{} // 点赞
	ViewsNum             interface{} // 浏览量
	MerchantReplyContent interface{} // 管理员回复内容
	MerchantReplyTime    interface{} // 管理员回复时间
	IsDel                interface{} // 0未删除1已删除
	IsReply              interface{} // 0未回复1已回复
	Nickname             interface{} // 用户名称
	Avatar               interface{} // 用户头像
}
