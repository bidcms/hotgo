// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductReplyComment is the golang structure of table hg_store_product_reply_comment for DAO operations like Where/Data.
type StoreProductReplyComment struct {
	g.Meta     `orm:"table:hg_store_product_reply_comment, do:true"`
	Id         interface{} //
	Uid        interface{} //
	Praise     interface{} // 点赞数量
	StoreId    interface{} // 门店id
	Content    interface{} // 回复内容
	Pid        interface{} // 上级回复id
	ReplyId    interface{} // 评论id
	CreateTime interface{} // 创建时间
	UpdateTime interface{} // 更新时间
}
