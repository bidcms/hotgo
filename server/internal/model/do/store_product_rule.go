// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductRule is the golang structure of table hg_store_product_rule for DAO operations like Where/Data.
type StoreProductRule struct {
	g.Meta    `orm:"table:hg_store_product_rule, do:true"`
	Id        interface{} //
	RuleName  interface{} // 规格名称
	RuleValue interface{} // 规格值
}
