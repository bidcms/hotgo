// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductSpecs is the golang structure of table hg_store_product_specs for DAO operations like Where/Data.
type StoreProductSpecs struct {
	g.Meta  `orm:"table:hg_store_product_specs, do:true"`
	Id      interface{} //
	Type    interface{} // 类型：1平台2:门店
	StoreId interface{} // 门店ID
	TempId  interface{} // 模版ID
	Name    interface{} // 参数名称
	Value   interface{} // 参数值
	Sort    interface{} // 排序
	Status  interface{} // 状态
	AddTime interface{} // 添加时间
}
