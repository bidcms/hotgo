// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductStockRecord is the golang structure of table hg_store_product_stock_record for DAO operations like Where/Data.
type StoreProductStockRecord struct {
	g.Meta    `orm:"table:hg_store_product_stock_record, do:true"`
	Id        interface{} //
	StoreId   interface{} // 门店ID
	ProductId interface{} // 商品ID
	Unique    interface{} // 规格唯一值
	CostPrice interface{} // 成本价
	Number    interface{} // 数量
	Pm        interface{} // 1:入库0:出库
	AddTime   interface{} // 时间
}
