// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductUnit is the golang structure of table hg_store_product_unit for DAO operations like Where/Data.
type StoreProductUnit struct {
	g.Meta  `orm:"table:hg_store_product_unit, do:true"`
	Id      interface{} //
	StoreId interface{} // 门店ID
	Name    interface{} // 单位名称
	Sort    interface{} // 排序
	Status  interface{} // 是否上架
	IsDel   interface{} // 是否删除
	AddTime interface{} // 添加时间
}
