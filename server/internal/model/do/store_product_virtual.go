// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
)

// StoreProductVirtual is the golang structure of table hg_store_product_virtual for DAO operations like Where/Data.
type StoreProductVirtual struct {
	g.Meta     `orm:"table:hg_store_product_virtual, do:true"`
	Id         interface{} // 主键id
	ProductId  interface{} // 商品id
	StoreId    interface{} // 门店id
	AttrUnique interface{} // 对应商品规格
	CardNo     interface{} // 卡密卡号
	CardPwd    interface{} // 卡密密码
	CardUnique interface{} // 虚拟卡密唯一值
	OrderId    interface{} // 购买订单id
	OrderType  interface{} // 购买订单类型：1:order 2:积分订单
	Uid        interface{} // 购买人id
}
