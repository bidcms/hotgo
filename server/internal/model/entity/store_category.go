// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreCategory is the golang structure for table store_category.
type StoreCategory struct {
	Id         int    `json:"id"         orm:"id"          description:"商品分类表ID"`
	Pid        int    `json:"pid"        orm:"pid"         description:"父id"`
	Type       int    `json:"type"       orm:"type"        description:"商品所属：0：平台1:门店2:供应商"`
	RelationId int    `json:"relationId" orm:"relation_id" description:"关联门店、供应商ID"`
	CateName   string `json:"cateName"   orm:"cate_name"   description:"分类名称"`
	Sort       int    `json:"sort"       orm:"sort"        description:"排序"`
	Pic        string `json:"pic"        orm:"pic"         description:"图标"`
	IsShow     int    `json:"isShow"     orm:"is_show"     description:"是否推荐"`
	AddTime    int    `json:"addTime"    orm:"add_time"    description:"添加时间"`
	BigPic     string `json:"bigPic"     orm:"big_pic"     description:"分类大图"`
}
