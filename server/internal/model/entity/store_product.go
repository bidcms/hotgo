// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProduct is the golang structure for table store_product.
type StoreProduct struct {
	Id               int     `json:"id"               orm:"id"                 description:"商品id"`
	Pid              int     `json:"pid"              orm:"pid"                description:"关联平台商品ID"`
	Type             int     `json:"type"             orm:"type"               description:"商品所属：0：平台1:门店2:供应商"`
	ProductType      int     `json:"productType"      orm:"product_type"       description:"商品类型0:普通商品，1：卡密，2：优惠券，3：虚拟商品"`
	ProductTypeText  string  `json:"productTypeText" orm:"product_type_text" description:"商品类型名称"`
	RelationId       int     `json:"relationId"       orm:"relation_id"        description:"关联门店、供应商ID"`
	MerId            uint    `json:"merId"            orm:"mer_id"             description:"商户Id(0为总后台管理员创建,不为0的时候是商户后台创建)"`
	Image            string  `json:"image"            orm:"image"              description:"商品图片"`
	RecommendImage   string  `json:"recommendImage"   orm:"recommend_image"    description:"推荐图"`
	SliderImage      string  `json:"sliderImage"      orm:"slider_image"       description:"轮播图"`
	StoreName        string  `json:"storeName"        orm:"store_name"         description:"商品名称"`
	StoreInfo        string  `json:"storeInfo"        orm:"store_info"         description:"商品简介"`
	Keyword          string  `json:"keyword"          orm:"keyword"            description:"关键字"`
	BarCode          string  `json:"barCode"          orm:"bar_code"           description:"商品条码（一维码）"`
	CateId           string  `json:"cateId"           orm:"cate_id"            description:"分类id"`
	Price            float64 `json:"price"            orm:"price"              description:"商品价格"`
	VipPrice         float64 `json:"vipPrice"         orm:"vip_price"          description:"会员价格"`
	OtPrice          float64 `json:"otPrice"          orm:"ot_price"           description:"市场价"`
	Postage          float64 `json:"postage"          orm:"postage"            description:"邮费"`
	DeliveryType     string  `json:"deliveryType"     orm:"delivery_type"      description:"商品配送方式"`
	Freight          int     `json:"freight"          orm:"freight"            description:"运费设置"`
	UnitName         string  `json:"unitName"         orm:"unit_name"          description:"单位名"`
	Sort             int     `json:"sort"             orm:"sort"               description:"排序"`
	Sales            uint    `json:"sales"            orm:"sales"              description:"销量"`
	Stock            uint    `json:"stock"            orm:"stock"              description:"库存"`
	IsShow           int     `json:"isShow"           orm:"is_show"            description:"状态（0：未上架，1：上架）"`
	IsHot            int     `json:"isHot"            orm:"is_hot"             description:"是否热卖"`
	IsBenefit        int     `json:"isBenefit"        orm:"is_benefit"         description:"是否优惠"`
	IsBest           int     `json:"isBest"           orm:"is_best"            description:"是否精品"`
	IsNew            int     `json:"isNew"            orm:"is_new"             description:"是否新品"`
	AddTime          uint    `json:"addTime"          orm:"add_time"           description:"添加时间"`
	IsPostage        uint    `json:"isPostage"        orm:"is_postage"         description:"是否包邮"`
	IsVerify         int     `json:"isVerify"         orm:"is_verify"          description:"是否审核：-2强制下架-1未通过0未审核1:通过"`
	IsDel            uint    `json:"isDel"            orm:"is_del"             description:"是否删除"`
	MerUse           uint    `json:"merUse"           orm:"mer_use"            description:"商户是否代理 0不可代理1可代理"`
	GiveIntegral     float64 `json:"giveIntegral"     orm:"give_integral"      description:"获得积分"`
	Cost             float64 `json:"cost"             orm:"cost"               description:"成本价"`
	IsSeckill        uint    `json:"isSeckill"        orm:"is_seckill"         description:"秒杀状态 0 未开启 1已开启"`
	IsBargain        uint    `json:"isBargain"        orm:"is_bargain"         description:"砍价状态 0未开启 1开启"`
	IsGood           int     `json:"isGood"           orm:"is_good"            description:"是否优品推荐"`
	IsSub            int     `json:"isSub"            orm:"is_sub"             description:"是否单独分佣"`
	IsVip            int     `json:"isVip"            orm:"is_vip"             description:"是否开启会员价格"`
	Ficti            int     `json:"ficti"            orm:"ficti"              description:"虚拟销量"`
	Browse           int     `json:"browse"           orm:"browse"             description:"浏览量"`
	CodePath         string  `json:"codePath"         orm:"code_path"          description:"商品二维码地址(用户小程序海报)"`
	SoureLink        string  `json:"soureLink"        orm:"soure_link"         description:"淘宝京东1688类型"`
	VideoLink        string  `json:"videoLink"        orm:"video_link"         description:"主图视频链接"`
	TempId           int     `json:"tempId"           orm:"temp_id"            description:"运费模板ID"`
	SpecType         int     `json:"specType"         orm:"spec_type"          description:"规格 0单 1多"`
	Activity         string  `json:"activity"         orm:"activity"           description:"活动显示排序1=秒杀，2=砍价，3=拼团"`
	Spu              string  `json:"spu"              orm:"spu"                description:"商品SPU"`
	LabelId          string  `json:"labelId"          orm:"label_id"           description:"标签ID"`
	CommandWord      string  `json:"commandWord"      orm:"command_word"       description:"复制口令"`
	RecommendList    string  `json:"recommendList"    orm:"recommend_list"     description:"推荐产品"`
	BrandId          int     `json:"brandId"          orm:"brand_id"           description:"品牌id"`
	BrandCom         string  `json:"brandCom"         orm:"brand_com"          description:"品牌组合"`
	Code             string  `json:"code"             orm:"code"               description:"编码"`
	IsVipProduct     int     `json:"isVipProduct"     orm:"is_vip_product"     description:"是否会员专属商品"`
	IsPresaleProduct int     `json:"isPresaleProduct" orm:"is_presale_product" description:"是否预售商品"`
	PresaleStartTime int     `json:"presaleStartTime" orm:"presale_start_time" description:"预售开始时间"`
	PresaleEndTime   int     `json:"presaleEndTime"   orm:"presale_end_time"   description:"预售结束时间"`
	PresaleDay       int     `json:"presaleDay"       orm:"presale_day"        description:"预售结束后几天内发货"`
	AutoOnTime       int     `json:"autoOnTime"       orm:"auto_on_time"       description:"自动上架时间"`
	AutoOffTime      int     `json:"autoOffTime"      orm:"auto_off_time"      description:"自动下架时间"`
	CustomForm       string  `json:"customForm"       orm:"custom_form"        description:"自定义表单"`
	IsSupportRefund  int     `json:"isSupportRefund"  orm:"is_support_refund"  description:"是否支持退款"`
	StoreLabelId     string  `json:"storeLabelId"     orm:"store_label_id"     description:"商品标签iDS"`
	EnsureId         string  `json:"ensureId"         orm:"ensure_id"          description:"商品保障服务ids"`
	Specs            string  `json:"specs"            orm:"specs"              description:"商品参数"`
	SpecsId          int     `json:"specsId"          orm:"specs_id"           description:"参数模版ID"`
	IsLimit          int     `json:"isLimit"          orm:"is_limit"           description:"是否开启限购"`
	LimitType        int     `json:"limitType"        orm:"limit_type"         description:"限购类型1单次限购2永久限购"`
	LimitNum         int     `json:"limitNum"         orm:"limit_num"          description:"限购数量"`
	Refusal          string  `json:"refusal"          orm:"refusal"            description:"审核拒绝原因"`
}
