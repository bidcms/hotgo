// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductAttr is the golang structure for table store_product_attr.
type StoreProductAttr struct {
	Id         int    `json:"id"         orm:"id"          description:""`
	ProductId  uint   `json:"productId"  orm:"product_id"  description:"商品ID"`
	AttrName   string `json:"attrName"   orm:"attr_name"   description:"属性名"`
	AttrValues string `json:"attrValues" orm:"attr_values" description:"属性值"`
	Type       int    `json:"type"       orm:"type"        description:"活动类型 0=商品，1=秒杀，2=砍价，3=拼团"`
}
