// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductAttrResult is the golang structure for table store_product_attr_result.
type StoreProductAttrResult struct {
	Id         int    `json:"id"         orm:"id"          description:""`
	ProductId  uint   `json:"productId"  orm:"product_id"  description:"商品ID"`
	Result     string `json:"result"     orm:"result"      description:"商品属性参数"`
	ChangeTime uint   `json:"changeTime" orm:"change_time" description:"上次修改时间"`
	Type       int    `json:"type"       orm:"type"        description:"活动类型 0=商品，1=秒杀，2=砍价，3=拼团"`
}
