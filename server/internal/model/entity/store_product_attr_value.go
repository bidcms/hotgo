// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductAttrValue is the golang structure for table store_product_attr_value.
type StoreProductAttrValue struct {
	Id           int     `json:"id"           orm:"id"            description:""`
	ProductId    uint    `json:"productId"    orm:"product_id"    description:"商品ID"`
	ProductType  int     `json:"productType"  orm:"product_type"  description:"商品类型"`
	Suk          string  `json:"suk"          orm:"suk"           description:"商品属性索引值 (attr_value|attr_value[|....])"`
	Stock        uint    `json:"stock"        orm:"stock"         description:"属性对应的库存"`
	SumStock     int     `json:"sumStock"     orm:"sum_stock"     description:"平台库存+门店库存总和"`
	Sales        uint    `json:"sales"        orm:"sales"         description:"销量"`
	Price        float64 `json:"price"        orm:"price"         description:"属性金额"`
	Image        string  `json:"image"        orm:"image"         description:"图片"`
	Unique       string  `json:"unique"       orm:"unique"        description:"唯一值"`
	Cost         float64 `json:"cost"         orm:"cost"          description:"成本价"`
	BarCode      string  `json:"barCode"      orm:"bar_code"      description:"商品条码"`
	OtPrice      float64 `json:"otPrice"      orm:"ot_price"      description:"原价"`
	VipPrice     float64 `json:"vipPrice"     orm:"vip_price"     description:"会员专享价"`
	Weight       float64 `json:"weight"       orm:"weight"        description:"重量"`
	Volume       float64 `json:"volume"       orm:"volume"        description:"体积"`
	Brokerage    float64 `json:"brokerage"    orm:"brokerage"     description:"一级返佣"`
	BrokerageTwo float64 `json:"brokerageTwo" orm:"brokerage_two" description:"二级返佣"`
	Type         int     `json:"type"         orm:"type"          description:"活动类型 0=商品，1=秒杀，2=砍价，3=拼团"`
	Quota        int     `json:"quota"        orm:"quota"         description:"活动限购数量"`
	QuotaShow    int     `json:"quotaShow"    orm:"quota_show"    description:"活动限购数量显示"`
	Code         string  `json:"code"         orm:"code"          description:"编码"`
	DiskInfo     string  `json:"diskInfo"     orm:"disk_info"     description:"虚拟信息内容"`
}
