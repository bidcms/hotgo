// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductCate is the golang structure for table store_product_cate.
type StoreProductCate struct {
	Id        int `json:"id"        orm:"id"         description:""`
	ProductId int `json:"productId" orm:"product_id" description:"商品id"`
	CateId    int `json:"cateId"    orm:"cate_id"    description:"分类id"`
	AddTime   int `json:"addTime"   orm:"add_time"   description:"添加时间"`
	CatePid   int `json:"catePid"   orm:"cate_pid"   description:"一级分类id"`
	Status    int `json:"status"    orm:"status"     description:"商品状态"`
}
