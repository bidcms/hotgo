// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductCoupon is the golang structure for table store_product_coupon.
type StoreProductCoupon struct {
	Id            int    `json:"id"            orm:"id"              description:""`
	ProductId     int    `json:"productId"     orm:"product_id"      description:"商品id"`
	IssueCouponId int    `json:"issueCouponId" orm:"issue_coupon_id" description:"优惠劵id"`
	AddTime       int    `json:"addTime"       orm:"add_time"        description:"添加时间"`
	Title         string `json:"title"         orm:"title"           description:"优惠券名称"`
}
