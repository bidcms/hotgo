// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductDescription is the golang structure for table store_product_description.
type StoreProductDescription struct {
	ProductId   int    `json:"productId"   orm:"product_id"  description:"商品ID"`
	Description string `json:"description" orm:"description" description:"商品详情"`
	Type        int    `json:"type"        orm:"type"        description:"商品类型"`
}
