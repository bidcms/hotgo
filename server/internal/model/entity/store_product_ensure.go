// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductEnsure is the golang structure for table store_product_ensure.
type StoreProductEnsure struct {
	Id      int    `json:"id"      orm:"id"       description:""`
	Type    int    `json:"type"    orm:"type"     description:"类型：1平台2:门店"`
	StoreId int    `json:"storeId" orm:"store_id" description:"门店ID"`
	Name    string `json:"name"    orm:"name"     description:"标签名称"`
	Image   string `json:"image"   orm:"image"    description:"图片"`
	Desc    string `json:"desc"    orm:"desc"     description:"描述"`
	Sort    int    `json:"sort"    orm:"sort"     description:"排序"`
	Status  int    `json:"status"  orm:"status"   description:"状态"`
	AddTime int    `json:"addTime" orm:"add_time" description:"添加时间"`
}
