// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductLabel is the golang structure for table store_product_label.
type StoreProductLabel struct {
	Id        int    `json:"id"        orm:"id"         description:""`
	Type      int    `json:"type"      orm:"type"       description:"类型：1平台2:门店"`
	StoreId   int    `json:"storeId"   orm:"store_id"   description:"门店ID"`
	LabelCate int    `json:"labelCate" orm:"label_cate" description:"标签分类"`
	LabelName string `json:"labelName" orm:"label_name" description:"标签名称"`
	Sort      int    `json:"sort"      orm:"sort"       description:"排序"`
	AddTime   int    `json:"addTime"   orm:"add_time"   description:"添加时间"`
}
