// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductLabelAuxiliary is the golang structure for table store_product_label_auxiliary.
type StoreProductLabelAuxiliary struct {
	Id        int `json:"id"        orm:"id"         description:""`
	LabelId   int `json:"labelId"   orm:"label_id"   description:""`
	ProductId int `json:"productId" orm:"product_id" description:""`
}
