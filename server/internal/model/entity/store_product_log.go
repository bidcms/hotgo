// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// StoreProductLog is the golang structure for table store_product_log.
type StoreProductLog struct {
	Id          int         `json:"id"          orm:"id"           description:""`
	Type        string      `json:"type"        orm:"type"         description:"类型"`
	ProductId   int         `json:"productId"   orm:"product_id"   description:"商品ID"`
	Uid         int         `json:"uid"         orm:"uid"          description:"用户ID"`
	VisitNum    int         `json:"visitNum"    orm:"visit_num"    description:"是否浏览"`
	CartNum     int         `json:"cartNum"     orm:"cart_num"     description:"加入购物车数量"`
	OrderNum    int         `json:"orderNum"    orm:"order_num"    description:"下单数量"`
	PayNum      int         `json:"payNum"      orm:"pay_num"      description:"支付数量"`
	PayPrice    float64     `json:"payPrice"    orm:"pay_price"    description:"支付金额"`
	CostPrice   float64     `json:"costPrice"   orm:"cost_price"   description:"商品成本价"`
	PayUid      int         `json:"payUid"      orm:"pay_uid"      description:"支付用户ID"`
	RefundNum   int         `json:"refundNum"   orm:"refund_num"   description:"退款数量"`
	RefundPrice float64     `json:"refundPrice" orm:"refund_price" description:"退款金额"`
	CollectNum  int         `json:"collectNum"  orm:"collect_num"  description:"收藏"`
	AddTime     int         `json:"addTime"     orm:"add_time"     description:"添加时间"`
	DeleteTime  *gtime.Time `json:"deleteTime"  orm:"delete_time"  description:"删除时间"`
}
