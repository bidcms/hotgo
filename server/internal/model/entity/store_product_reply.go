// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductReply is the golang structure for table store_product_reply.
type StoreProductReply struct {
	Id                   int    `json:"id"                   orm:"id"                     description:"评论ID"`
	Uid                  int    `json:"uid"                  orm:"uid"                    description:"用户ID"`
	Oid                  int    `json:"oid"                  orm:"oid"                    description:"订单ID"`
	Type                 int    `json:"type"                 orm:"type"                   description:"类型：1平台2:门店"`
	StoreId              int    `json:"storeId"              orm:"store_id"               description:"门店id"`
	Unique               string `json:"unique"               orm:"unique"                 description:"唯一id"`
	ProductId            int    `json:"productId"            orm:"product_id"             description:"商品id"`
	ReplyType            string `json:"replyType"            orm:"reply_type"             description:"某种商品类型(普通商品、秒杀商品）"`
	ProductScore         int    `json:"productScore"         orm:"product_score"          description:"商品分数"`
	ServiceScore         int    `json:"serviceScore"         orm:"service_score"          description:"服务分数"`
	Comment              string `json:"comment"              orm:"comment"                description:"评论内容"`
	Pics                 string `json:"pics"                 orm:"pics"                   description:"评论图片"`
	AddTime              int    `json:"addTime"              orm:"add_time"               description:"评论时间"`
	Praise               int    `json:"praise"               orm:"praise"                 description:"点赞"`
	ViewsNum             int    `json:"viewsNum"             orm:"views_num"              description:"浏览量"`
	MerchantReplyContent string `json:"merchantReplyContent" orm:"merchant_reply_content" description:"管理员回复内容"`
	MerchantReplyTime    int    `json:"merchantReplyTime"    orm:"merchant_reply_time"    description:"管理员回复时间"`
	IsDel                uint   `json:"isDel"                orm:"is_del"                 description:"0未删除1已删除"`
	IsReply              int    `json:"isReply"              orm:"is_reply"               description:"0未回复1已回复"`
	Nickname             string `json:"nickname"             orm:"nickname"               description:"用户名称"`
	Avatar               string `json:"avatar"               orm:"avatar"                 description:"用户头像"`
}
