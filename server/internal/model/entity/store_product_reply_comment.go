// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductReplyComment is the golang structure for table store_product_reply_comment.
type StoreProductReplyComment struct {
	Id         int    `json:"id"         orm:"id"          description:""`
	Uid        int    `json:"uid"        orm:"uid"         description:""`
	Praise     int    `json:"praise"     orm:"praise"      description:"点赞数量"`
	StoreId    int    `json:"storeId"    orm:"store_id"    description:"门店id"`
	Content    string `json:"content"    orm:"content"     description:"回复内容"`
	Pid        int    `json:"pid"        orm:"pid"         description:"上级回复id"`
	ReplyId    int    `json:"replyId"    orm:"reply_id"    description:"评论id"`
	CreateTime int    `json:"createTime" orm:"create_time" description:"创建时间"`
	UpdateTime int    `json:"updateTime" orm:"update_time" description:"更新时间"`
}
