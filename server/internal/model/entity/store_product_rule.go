// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductRule is the golang structure for table store_product_rule.
type StoreProductRule struct {
	Id        int    `json:"id"        orm:"id"         description:""`
	RuleName  string `json:"ruleName"  orm:"rule_name"  description:"规格名称"`
	RuleValue string `json:"ruleValue" orm:"rule_value" description:"规格值"`
}
