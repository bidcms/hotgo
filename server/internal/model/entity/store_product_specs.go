// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductSpecs is the golang structure for table store_product_specs.
type StoreProductSpecs struct {
	Id      int    `json:"id"      orm:"id"       description:""`
	Type    int    `json:"type"    orm:"type"     description:"类型：1平台2:门店"`
	StoreId int    `json:"storeId" orm:"store_id" description:"门店ID"`
	TempId  int    `json:"tempId"  orm:"temp_id"  description:"模版ID"`
	Name    string `json:"name"    orm:"name"     description:"参数名称"`
	Value   string `json:"value"   orm:"value"    description:"参数值"`
	Sort    int    `json:"sort"    orm:"sort"     description:"排序"`
	Status  int    `json:"status"  orm:"status"   description:"状态"`
	AddTime int    `json:"addTime" orm:"add_time" description:"添加时间"`
}
