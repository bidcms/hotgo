// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductStockRecord is the golang structure for table store_product_stock_record.
type StoreProductStockRecord struct {
	Id        int     `json:"id"        orm:"id"         description:""`
	StoreId   int     `json:"storeId"   orm:"store_id"   description:"门店ID"`
	ProductId int     `json:"productId" orm:"product_id" description:"商品ID"`
	Unique    string  `json:"unique"    orm:"unique"     description:"规格唯一值"`
	CostPrice float64 `json:"costPrice" orm:"cost_price" description:"成本价"`
	Number    int     `json:"number"    orm:"number"     description:"数量"`
	Pm        int     `json:"pm"        orm:"pm"         description:"1:入库0:出库"`
	AddTime   int     `json:"addTime"   orm:"add_time"   description:"时间"`
}
