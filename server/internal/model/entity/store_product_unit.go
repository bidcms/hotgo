// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductUnit is the golang structure for table store_product_unit.
type StoreProductUnit struct {
	Id      int    `json:"id"      orm:"id"       description:""`
	StoreId int    `json:"storeId" orm:"store_id" description:"门店ID"`
	Name    string `json:"name"    orm:"name"     description:"单位名称"`
	Sort    int    `json:"sort"    orm:"sort"     description:"排序"`
	Status  int    `json:"status"  orm:"status"   description:"是否上架"`
	IsDel   int    `json:"isDel"   orm:"is_del"   description:"是否删除"`
	AddTime int    `json:"addTime" orm:"add_time" description:"添加时间"`
}
