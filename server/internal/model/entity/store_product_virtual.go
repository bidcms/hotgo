// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

// StoreProductVirtual is the golang structure for table store_product_virtual.
type StoreProductVirtual struct {
	Id         uint   `json:"id"         orm:"id"          description:"主键id"`
	ProductId  int    `json:"productId"  orm:"product_id"  description:"商品id"`
	StoreId    int    `json:"storeId"    orm:"store_id"    description:"门店id"`
	AttrUnique string `json:"attrUnique" orm:"attr_unique" description:"对应商品规格"`
	CardNo     string `json:"cardNo"     orm:"card_no"     description:"卡密卡号"`
	CardPwd    string `json:"cardPwd"    orm:"card_pwd"    description:"卡密密码"`
	CardUnique string `json:"cardUnique" orm:"card_unique" description:"虚拟卡密唯一值"`
	OrderId    string `json:"orderId"    orm:"order_id"    description:"购买订单id"`
	OrderType  int    `json:"orderType"  orm:"order_type"  description:"购买订单类型：1:order 2:积分订单"`
	Uid        int    `json:"uid"        orm:"uid"         description:"购买人id"`
}
