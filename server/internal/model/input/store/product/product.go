package product

import (
	"context"
	"hotgo/internal/model/entity"
	"hotgo/internal/model/input/form"

	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gmeta"
)

// Hook function definition.
var Hook = gdb.HookHandler{
	Select: func(ctx context.Context, in *gdb.HookSelectInput) (result gdb.Result, err error) {
		result, err = in.Next(ctx)
		if err != nil {
			return
		}
		types := g.Slice{"普通商品", "卡密", "优惠券", "虚拟商品"}
		for i, record := range result {
			record["product_type_text"] = gvar.New(types[record["product_type"].Int()])
			result[i] = record
		}
		return
	},
}

type StoreProductAttr struct {
	gmeta.Meta `orm:"table:store_product_attr"`
	Id         int    `json:"id" orm:"id" dc:"ID"`
	AttrName   string `json:"attrName" orm:"attr_name" dc:"属性名"`
	AttrValues string `json:"attrValues" orm:"attr_values" dc:"属性值"`
}
type StoreProductListInp struct {
	form.PageReq
	Keyword      string `json:"keyword"  dc:"搜索关键字"`
	Sid          int    `json:"sid"  dc:"分类ID"`
	Cid          int    `json:"cid"  dc:"分类ID"`
	PriceOrder   string `json:"priceOrder" dc:"价格排序"`
	SalesOrder   string `json:"salesOrder" dc:"销量排序"`
	New          int    `json:"new" dc:"是否新品"`
	Type         int    `json:"type" dc:"商品状态"`
	Ids          string `json:"ids" dc:"商品ID集合"`
	ProductId    string `json:"productId" dc:"商品ID"`
	BrandId      string `json:"brandId" dc:"品牌ID"`
	PromotionsId int    `json:"promotionsId" dc:"活动ID"`
}

type StoreProduct struct {
	entity.StoreProduct
	Attribute []*StoreProductAttr `json:"attribute" orm:"with:product_id=id" dc:"商品参数"`
}

func (in *StoreProductListInp) Filter(ctx context.Context) (err error) {
	return
}

func (out *StoreProduct) GetStatusAttribute(value string) {

}
